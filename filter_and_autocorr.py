# -*- coding: utf-8 -*-
"""
Created on Wed Jun 28 11:09:33 2023

@author: wiesbrock
"""

import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import zscore
import seaborn as sns
import os


def crosscorr(datax, datay, lag=0):
    """ Lag-N cross correlation. 
    Parameters
    ----------
    lag : int, default 0
    datax, datay : pandas.Series objects of equal length
    Returns
    ----------
    crosscorr : float
    """
    return datax.corr(datay.shift(lag))


path=r'Y:\File transfer\Christopher_transfer\von Jerome\Excel-sheets\Deep_interpolation_Traces_Str8_MoveBasedOnRaw.xlsx'
sheet_name='20220425_Seq001'
data=pd.read_excel(path, sheet_name=sheet_name)
names=data.columns
names=names[1:]
os.chdir(r'Y:\File transfer\Christopher_transfer\von Jerome\Excel-sheets')
try:
    #os.mkdir('HP')
    os.mkdir('HP/'+str(sheet_name))
    #os.mkdir('TP')
    #os.mkdir('TP/'+str(sheet_name))
except:
    print('Folder already exists')



for i in range(len(names)):
    # Erzeugung von Beispieldaten
    fs = 2  # Abtastrate (Sample Rate)
    t = np.linspace(0, 1, fs, endpoint=False)  # Zeitachse
    x = data[names[i]] # Beispiel-Signal mit zwei Frequenzen
    x=np.array(x)
    x=x[~np.isnan(x)]
    #x=x.fillna(np.mean(x)) 
    x=zscore(x)
    n=60
    n=np.linspace(0-n,n,n*2+1)
    n=n.astype(int)
    autocorr=np.zeros((len(n)))
    x_df=pd.DataFrame(x)
    x_df=x_df.squeeze()
    t=0
    for m in n:
        
        autocorr[t]=crosscorr(x_df,x_df,lag=m)
        t=t+1

    # Berechnung des Power-Spektrums
    f, Pxx = signal.periodogram(x, fs)
    
    binary=np.zeros((len(Pxx)))
    binary[Pxx>0.1*np.max(Pxx)]=1
    kernel_size = 9
    kernel = np.ones(kernel_size) / kernel_size
    binary = np.convolve(binary, kernel, mode='same')
    binary[binary>0]=1
    diffbin=np.diff(binary)
    where_diff=np.where(diffbin==-1)

    # Bestimmung des Grenzwerts des Peaks im Periodogramm
    peak_freq = f[where_diff]
    cutoff_freq = peak_freq  # Grenzfrequenz des Hochpassfilters

    # Design des Hochpassfilters
    order = 4  # Filterordnung
    b, a = signal.butter(order, cutoff_freq, btype='high', fs=fs)

    # Anwenden des Hochpassfilters auf das Eingangssignal
    filtered_x = signal.filtfilt(b, a, x)

    # Berechnung des Power-Spektrums des gefilterten Signals
    f_filtered, Pxx_filtered = signal.periodogram(filtered_x, fs)
    
    filtered_x=zscore(filtered_x)

    # Plotten des Power-Spektrums
    plt.figure(dpi=300)
    
    plt.subplot(3, 1, 1)
    plt.plot(x)
    plt.title(names[i]+' input signal')
    sns.despine()

    plt.subplot(3, 1, 2)
    plt.plot(f, Pxx)
    plt.xlabel('Frequenzy [Hz]')
    plt.ylabel('Power density')
    plt.xlim(0,0.05)
    plt.title('Power-Spectrum')
    sns.despine()

    plt.subplot(3, 1, 3)
    plt.plot(zscore(filtered_x))
    plt.title('Output signal')
    sns.despine()
    
    plt.figure()
    plt.plot(n,autocorr)
    plt.ylabel('Autocorrelation [AU]')
    plt.xlabel('Lag[Frames]')
    sns.despine()
    
    
    #plt.subplot(2, 1, 2)
    #plt.plot(f_filtered,Pxx_filtered)
    #plt.title('Ausgangssignal')

    plt.tight_layout()
    plt.savefig('HP/'+str(sheet_name)+'/'+str(names[i])+'.png')
    plt.show()
