# -*- coding: utf-8 -*-
"""
Created on Thu Jun 15 13:35:45 2023

@author: wiesbrock
"""

import scipy.stats as stats
import numpy
import seaborn as sns
import pandas as pd
import numpy as np
import math
import seaborn as sns
import matplotlib.pylab as plt

path=r'Y:\File transfer\Christopher_transfer\von Jerome\Excel-sheets\Deep_interpolation_Traces_Str8_MoveBasedOnRaw.xlsx'


df=pd.read_excel(path)

name=df.columns

data=df[name[15]]

plt.figure()
plt.subplot(411)
plt.plot(data)

signal=df[name[15]]

from scipy.signal import butter, lfilter, welch

def apply_highpass_filter(input_signal, cutoff_freq, sampling_freq, filter_order=5):
    nyquist_freq = 0.5 * sampling_freq
    normalized_cutoff_freq = cutoff_freq / nyquist_freq

    # Butterworth-Filterkoeffizienten berechnen
    b, a = butter(filter_order, normalized_cutoff_freq, btype='high', analog=False, output='ba')

    # Filter auf das Eingangssignal anwenden
    filtered_signal = lfilter(b, a, input_signal)

    return filtered_signal
sampling_freq = 2
signal = np.array(signal)
signal = signal[~np.isnan(signal)]

frequencies, power_spectrum = welch(signal, fs=sampling_freq)

plt.figure(figsize=(8, 5))
plt.plot(frequencies, power_spectrum)
plt.title('Power Spectrum')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power')
plt.grid(True)
plt.show()
# Beispielwerte für das Eingangssignal und Filterparameter

n=2
for i in [0.01,0.05,0.1]:
    cutoff_frequency = i  # Cutoff-Frequenz in Anteilen der Abtastfrequenz
    

    # Highpass-Filter anwenden
    filtered_signal = apply_highpass_filter(signal, cutoff_frequency, sampling_freq)

    plt.subplot(410+int(n))
    plt.plot(filtered_signal, label=i)
    plt.legend()
    
    n=n+1
    
frequencies, power_spectrum = welch(filtered_signal, fs=sampling_freq)

plt.figure(figsize=(8, 5))
plt.plot(frequencies, power_spectrum)
plt.title('Power Spectrum')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power')
plt.grid(True)
plt.show()
    




